#!/usr/bin/env python

# example config.py file 
# settings = {
#     'userid': 'username',
#     'passwd': 'password',
# }

import os
import glob
import config
from time import sleep
from selenium import webdriver
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.firefox.options import Options
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC 
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


def get_browser():
    options = Options()
    options.add_argument("--headless")
    options.add_argument("--window-size=1920x1200")
    driver = webdriver.Firefox(options=options, service=FirefoxService(GeckoDriverManager().install()))

    return driver


def login(browser) -> int:

    print('= logging in')
    # open the page in browser
    browser.get('https://tech3330.visualtime.net/VTPortal/2/indexv2.aspx#login')
    # wait for "enviar"-element to load, page should be fully loaded then
    WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/div[2]/div/span')))

    try: 
        print(" = entering login name")
        login1 = browser.find_element(By.XPATH, '/html/body/div[2]/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/div[1]/div[1]/div/div/input')
        login1.send_keys(config.settings['userid'])

        print(" = entering password")
        login1 = browser.find_element(By.XPATH, '/html/body/div[2]/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/div[1]/div[2]/div[1]/div/input')
        login1.send_keys(config.settings['passwd'])

        print(" = click enviar")
        browser.find_element(By.XPATH, '/html/body/div[2]/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/div[2]/div').click()

    except:
        return 1

    return 0


def action(browser):
    browser.get('https://tech3330.visualtime.net/VTPortal/2/indexv2.aspx#home')

    # find the activity button, could be either 'entrar' or 'salir'
    activity = browser.find_element(By.XPATH, '/html/body/div[2]/div[1]/div[2]/div/div[5]/div[2]/div/div/div/span')
    action = ActionChains(browser)
    if (activity.text == 'salir'):
        print("# press salir - end the working day")
        action.click_and_hold(on_element = activity)
    else:
        print("# press entrar - start the working day")
        action.click_and_hold(on_element = activity)
    action.perform()

    print('= waiting 5secs for the page to become available')
    sleep(5)


def cleanup() -> int:
    os.remove("geckodriver.log")
    return 0


def main() -> int:
    # removing remnants from former runs, espacially the geckodriver.log
    cleanup()

    # initializing a browser instance
    browser = get_browser()

    # log in to the website
    if login(browser) != 0:
        print("--> login failed, exiting")
        browser.close()
        browser.quit()
        exit()

    # pressing the action button.
    print('= waiting 15secs for the page to become available')
    sleep(15)
    action(browser)

    # shutting down the browser instance
    browser.close()
    browser.quit()

    return 0


if __name__ == "__main__":
    main()
